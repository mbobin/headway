# frozen_string_literal: true

module Headway
  class MergeRequestReportItem
    attr_reader :merge_request

    def initialize(merge_request)
      @merge_request = merge_request
    end

    def emoji
      case merge_request.state
      when 'opened'
        ':hourglass_flowing_sand:'
      when 'merged'
        merged_state_emoji
      else
        ':information_source:'
      end
    end

    def title
      merge_request.title
    end

    def dri
      "`@#{merge_request.author.username}`"
    end

    def reference
      merge_request.references.full
    end

    def milestone
      "%#{merge_request.milestone&.title}" if merge_request.milestone
    end

    def impact
      desc = merge_request.description.to_s
      return if desc.empty?

      matches = desc.match(/^## Impact$\n+(.*)$/i)
      return unless matches

      matches[1]
    end

    def to_row
      values = [
        emoji,
        title,
        reference,
        milestone,
        dri,
        impact
      ].join(' | ')

      "| #{values} |"
    end

    private

    def merged_state_emoji
      if docs_only_mr?
        ':information_source:'
      else
        ':white_check_mark:'
      end
    end

    def docs_only_mr?
      merge_request.labels.include?('documentation') &&
        !merge_request.labels.include?('backend')
    end
  end
end
