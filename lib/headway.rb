# frozen_string_literal: true

require 'logger'
require 'gitlab'

require_relative 'headway/issue_report_item'
require_relative 'headway/merge_request_report_item'
require_relative 'headway/processor'

module Headway
  class Error < StandardError; end
end
