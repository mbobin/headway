# Headway

A tool for tracking progress for CI time decay

Heavily inspired by https://gitlab.com/nolith/review-tanuki

## Usage

### Setup

Get yourself an [API private token](https://gitlab.com/-/profile/personal_access_tokens) with the "api" scope.
You can now run it using one of the following methods:

1. Locally (e.g. on a Raspberry Pi or a laptop)
1. On a server
2. On GitLab CI, using CI schedules

In all cases, it comes down to running the following command:

```
env HEADWAY_EPIC_REFERENCE='group&iid' GITLAB_API_PRIVATE_TOKEN='...' GITLAB_API_ENDPOINT='https://gitlab.com/api/v4' bundle exec ./bin/headway
```

### Configuration options

- `HEADWAY_EPIC_REFERENCE` - the epic that will be updated
- `HEADWAY_DRY_RUN` - skips epic update when present
- `HEADWAY_TRACKING_LABEL` - the label that will be used for filtering merge requests and issues
